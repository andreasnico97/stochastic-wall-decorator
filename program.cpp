#include <iostream>
#include "StochLib.h"
using namespace std;

int main() {
  double a = 500.0 / 365.0;
  Variable<int>* x = new Poisson(a);
  // cout << x->getExpectedValue() << endl;
  cout << "Poisson(1): " << x->PMF(1) << endl;

  Variable<int>* b = new Binomial(500, (1.0/365.0));
  double p = b->PMF(1);
  cout << "Binomial(1): " << p << endl;

  return 0;
}
