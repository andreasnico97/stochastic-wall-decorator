make: program.o StochLib.o
	g++ -Wall program.o StochLib.o -o main

program.o: program.cpp
	g++ -c program.cpp

StochLib.o: StochLib.cpp StochLib.h
	g++ -c StochLib.cpp

clean:
	rm main *.o
