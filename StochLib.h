template <class t> class Variable
{
 public:
  virtual double getExpectedValue()=0;
  virtual double PMF(t)=0;
};

class Poisson : public Variable<int>
{
  double lambda;
 public:
  double getExpectedValue();
  double PMF(int);
  Poisson(double);
};

class Binomial : public Variable<int>
{
  double p;
  double n;
 public:
  double getExpectedValue();
  double PMF(int);
  Binomial(double, double);
};
