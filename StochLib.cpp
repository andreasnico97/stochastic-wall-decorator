#include "StochLib.h"
#include <math.h>
#include <iostream>

using namespace std;

int nChooseK(int n, int k)
{
  if (k == 0) return 1;
  return (n * nChooseK(n - 1, k - 1)) / k;
}

int factorial(int n)
{
  return (n == 1 || n == 0) ? 1 : factorial(n - 1) * n;
}



Poisson::Poisson(double lambda) : lambda(lambda)
{
}

double Poisson::PMF(int x)
{
  return (pow(lambda, x) * exp(-lambda)) / (factorial(x));
}

double Poisson::getExpectedValue()
{
  return lambda;
}

double Binomial::PMF(int k)
{
  int nk = nChooseK(n, k);
  double pk = pow(p, (double)k);
  double a = pow((1.0 - p), (n-(double)k));
  return (nk * pk * a);
}

double Binomial::getExpectedValue()
{
  return n*p;
}

Binomial::Binomial(double n, double p) : n(n), p(p)
{
}
